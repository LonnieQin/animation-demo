//
//  CircleView.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView
- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
    }
    return self;
}

- (void) drawRect:(CGRect)rect
{
    [[UIColor redColor] setFill];
    [[UIBezierPath bezierPathWithOvalInRect:self.bounds] fill];
}
@end
