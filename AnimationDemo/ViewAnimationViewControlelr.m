//
//  ViewAnimationViewControlelr.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ViewAnimationViewControlelr.h"
#import "CircleView.h"
@implementation ViewAnimationViewControlelr

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.circleView = [[CircleView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [self.view addSubview:self.circleView];
    UITapGestureRecognizer * t;
    t = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropAnimate:)];
    [self.view addGestureRecognizer:t];
}

- (void) dropAnimate:(UIGestureRecognizer*) g
{
    [UIView animateWithDuration:3 animations:^{
        g.enabled = NO;
        self.circleView.center = CGPointMake(100,300);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            self.circleView.center = CGPointMake(250, 300);
        } completion:^(BOOL finished) {
            g.enabled = YES;
        }];
    }];
}

@end
