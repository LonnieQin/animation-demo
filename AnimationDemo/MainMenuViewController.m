//
//  MainMenuViewController.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "MainMenuViewController.h"

@implementation MainMenuViewController
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id) sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        UITableViewCell * cell = (id) sender;
        UIViewController * controller = segue.destinationViewController;
        controller.title = cell.textLabel.text;
    }
}

@end
