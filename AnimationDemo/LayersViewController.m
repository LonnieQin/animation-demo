//
//  LayersViewController.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "LayersViewController.h"
#import "DelegateView.h"
@implementation LayersViewController
- (void) viewDidLoad
{
    [super viewDidLoad];
    UIImage * image = [UIImage imageNamed:@"pushing"];
    self.view.layer.contentsScale = [[UIScreen mainScreen] scale];
    self.view.layer.contentsGravity = kCAGravityCenter;
    self.view.layer.contents = (id) [image CGImage];
    
    UIGestureRecognizer * g;
    g = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(performFlip:)];
    [self.view addGestureRecognizer:g];
}

- (void) performFlip:(UIGestureRecognizer*) g
{
    UIView * delegateView = [[DelegateView alloc] initWithFrame:self.view.frame];
    [UIView transitionFromView:self.view toView:delegateView duration:1 options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
}
@end
