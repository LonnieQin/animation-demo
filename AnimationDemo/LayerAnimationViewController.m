//
//  LayerAnimationViewController.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "LayerAnimationViewController.h"

@implementation LayerAnimationViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    CALayer * squarelayer = [CALayer layer];
    squarelayer.backgroundColor = [[UIColor redColor] CGColor];
    squarelayer.frame = CGRectMake(100, 100, 20, 20);
    [self.view.layer addSublayer:squarelayer];
    
    UIView * squareView = [UIView new];
    squareView.tag = 100;
    squareView.backgroundColor = [UIColor blueColor];
    squareView.frame = CGRectMake(200, 100, 20, 20);
    [self.view addSubview:squareView];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(drop:)]];
}

- (void) drop:(UIGestureRecognizer*) g
{
    [CATransaction setAnimationDuration:2.0];
    NSArray * layers = self.view.layer.sublayers;
    CALayer * layer = [layers firstObject];
    CGPoint toPoint = CGPointMake(200, 250);
    layer.position = toPoint;
    
    CABasicAnimation * anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    anim.fromValue = @1.0;
    anim.toValue = @0.0;
    anim.autoreverses = YES;
    anim.repeatCount = INFINITY;
    anim.duration = 2.0;
    [layer addAnimation:anim forKey:@"anim"];
    
    UIView * view = [self.view viewWithTag:100];
    NSLog(@"%@",view);
    [view setCenter:CGPointMake(100, 250)];
 
}
@end
