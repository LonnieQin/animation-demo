//
//  DelegateView.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DelegateView.h"

@implementation DelegateView

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.layer setNeedsDisplay];
        [self.layer setContentsScale:[[UIScreen mainScreen] scale]];
    }
    return self;
}


- (void) drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    NSLog(@"%s",__func__);
    UIGraphicsPushContext(ctx);
    [[UIColor whiteColor] set];
    UIRectFill(layer.bounds);
    
    UIFont * font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    UIColor * color = [UIColor purpleColor];
    NSMutableParagraphStyle * style = [NSMutableParagraphStyle new];
    [style setAlignment:NSTextAlignmentCenter];
    
    NSDictionary * attrs = @{NSFontAttributeName:font,NSForegroundColorAttributeName:color,NSParagraphStyleAttributeName:style  };
    
    NSAttributedString * text = [[NSAttributedString alloc] initWithString:@"Pushiing The Limits" attributes:attrs];
    [text drawInRect:CGRectInset([layer bounds], 10, 100)];
    UIGraphicsPopContext();
}

@end
