//
//  ActionsViewController.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ActionsViewController.h"
#import "CircleLayer.h"
@interface ActionsViewController()
{
    CircleLayer * circleLayer;
}
@end
@implementation ActionsViewController
- (void) viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"1");
    circleLayer = [CircleLayer new];
    circleLayer.radius = 20;
    circleLayer.frame = self.view.bounds;
    [self.view.layer addSublayer:circleLayer];
    NSLog(@"2");
    CABasicAnimation * anim = [CABasicAnimation animationWithKeyPath:@"position"];
    anim.duration = 2;
    NSMutableDictionary * actions = [NSMutableDictionary dictionaryWithDictionary:[circleLayer actions]];
    actions[@"position"] = anim;
    NSLog(@"3");
    CABasicAnimation * fadeAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnim.fromValue = @0.4;
    fadeAnim.toValue = @1.0;
    NSLog(@"4");
    CABasicAnimation * growAnim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    growAnim.fromValue = @0.8;
    growAnim.toValue = @1.0;
    NSLog(@"5");
    CAAnimationGroup * groupAnim = [CAAnimationGroup animation];
    groupAnim.animations = @[fadeAnim,growAnim];
    
    actions[kCAOnOrderIn] = groupAnim;
    NSLog(@"6");
    circleLayer.actions = actions;
    
    UIGestureRecognizer * g = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer:g];
}

- (void) tap:(UIGestureRecognizer*) g
{
    int width = [UIScreen mainScreen].bounds.size.width;
    int height = [UIScreen mainScreen].bounds.size.height;
    circleLayer.position = CGPointMake(rand()%width, rand()%height);
    [CATransaction setAnimationDuration:2];
    circleLayer.radius = 100.0;
}
@end
