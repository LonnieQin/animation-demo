//
//  BoxViewController.m
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "BoxViewController.h"

@implementation BoxViewController
const CGFloat kSize = 100.;
const CGFloat kPanScale = 1./100.;

- (CALayer*) layerWithColor:(UIColor*) color transform:(CATransform3D) transform
{
    CALayer * layer = [CALayer new];
    layer.backgroundColor = [color CGColor];
    layer.bounds = CGRectMake(0, 0, kSize, kSize);
    layer.position = self.view.center;
    layer.transform = transform;
    [self.view.layer addSublayer:layer];
    return layer;
}

static CATransform3D MakePerspectiveTransform()
{
    CATransform3D perspective = CATransform3DIdentity;
    perspective.m34 = -1./2000;
    return perspective;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    CATransform3D transform;
    transform = CATransform3DMakeTranslation(0, -kSize/2, 0);
    transform = CATransform3DRotate(transform, M_PI_2, 1.0, 0, 0);
    self.topLayer = [self layerWithColor:[UIColor redColor] transform:transform];
    
    transform = CATransform3DMakeTranslation(0, kSize/2, 0);
    transform = CATransform3DRotate(transform, M_PI_2, 1.0, 0, 0);
    self.bottomLayer = [self layerWithColor:[UIColor greenColor] transform:transform];
    
    transform = CATransform3DMakeTranslation(kSize/2, 0, 0);
    transform = CATransform3DRotate(transform, M_PI_2, 0, 1, 0);
    self.rightLayer = [self layerWithColor:[UIColor blueColor] transform:transform];
    
    transform = CATransform3DMakeTranslation(-kSize/2, 0, 0);
    transform = CATransform3DRotate(transform, M_PI_2, 0, 1, 0);
    self.leftLayer = [self layerWithColor:[UIColor cyanColor] transform:transform];
    
    transform = CATransform3DMakeTranslation(0, 0, kSize/2);
    transform = CATransform3DRotate(transform, M_PI_2, 0, 0, 0);
    self.frontLayer =  [self layerWithColor:[UIColor magentaColor] transform:transform];
    
    transform = CATransform3DMakeTranslation(0, 0, -kSize/2);
    transform = CATransform3DRotate(transform, M_PI_2, 0, 0, 0);
    self.backLayer = [self layerWithColor:[UIColor blackColor] transform:transform];
    
    self.view.layer.sublayerTransform = MakePerspectiveTransform();
    
    UIPanGestureRecognizer * p = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self.view addGestureRecognizer:p];
}


- (void) pan:(UIPanGestureRecognizer*) p
{
    CGPoint translation = [p translationInView:self.view];
    CATransform3D transform = MakePerspectiveTransform();
    transform = CATransform3DRotate(transform, kPanScale*translation.x, 0, 1, 0);
    transform = CATransform3DRotate(transform, -kPanScale * translation.y, 1, 0, 0);
    self.view.layer.sublayerTransform = transform;
}

@end
