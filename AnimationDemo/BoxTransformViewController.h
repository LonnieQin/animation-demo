//
//  BoxTransformViewController.h
//  AnimationDemo
//
//  Created by amttgroup on 15-5-19.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoxTransformViewController : UIViewController
@property (nonatomic,readwrite,strong) CALayer * contentLayer;
@property (nonatomic,readwrite,strong) CALayer * topLayer;
@property (nonatomic,readwrite,strong) CALayer * bottomLayer;
@property (nonatomic,readwrite,strong) CALayer * leftLayer;
@property (nonatomic,readwrite,strong) CALayer * rightLayer;
@property (nonatomic,readwrite,strong) CALayer * frontLayer;
@property (nonatomic,readwrite,strong) CALayer * backLayer;
@end
